package com.fg.allevents.apirest.models.enums;

public enum DominioSexo {

    MASCULINO("M"),

    FEMININO("F"),

    OUTROS("O");

    private String id;

    private DominioSexo() {
    }

    private DominioSexo(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
